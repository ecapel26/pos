#include "OrderForm.h"
#include "OrderTicket.h"
#include "MenuItem.h"
#include "ui_OrderForm.h"
#include <string>
#include <QListWidget>
#include <QListWidgetItem>

OrderForm::OrderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrderForm)
{
    ui->setupUi(this);
}

OrderForm::~OrderForm()
{
    delete ui;
}

/************************************************************************
 * Public slots
 ************************************************************************/

void OrderForm::setTable(int table, int emp){
    setEmpNum(emp);
    setTableNum(table);
    showOrderPage();
    //TODO need to setup OrderTicket item 'ticket'

}

/************************************************************************
 * Private slots
 ************************************************************************/

void OrderForm::on_AddItemButton_clicked()
{
    ui->listWidget;
    QString item = ui->MenuItemEdit->text();
    int idNum = item.toInt();
    if(item.isEmpty())
        return;
//TODO validate MenuItem
//workaround - the price is pulled from the name only when the name is int or double
    double price = item.toDouble();
    MenuItem menuItem;
    //QString id = ui->MenuItemEdit->text();

    //add the item to the GUI
    int quantity = ui->quantSpin->value();
    item += ", x" + QString::number(quantity);
    int customer = ui->custSpin->value();
    item += ", cust #" + QString::number(customer);
    new QListWidgetItem(item, ui->listWidget, 0);
    //add the item to the ticket object
    menuItem.setItemId(idNum);
    menuItem.setItemPrice(price);
    ticket.addItemToOrder(customer, menuItem);
    //recalculate order
//this doesnt work until MenuItem is fixed
    //calculateTotals(ticket.calculateOrder());
//workaround - just pull the subtotal from the gui and add current item
    QString subtotal = ui->SubtotalText->text();
    subtotal.remove(0,1);
    double subt = subtotal.toDouble();
    calculateTotals(subt + price);

}

void OrderForm::on_RemoveItemButton_clicked()
{
    int row = ui->listWidget->currentRow();
    QListWidgetItem * rem = ui->listWidget->takeItem(row);
    //remove the item from the ticket
    QString item = rem->text();
    for(int i = 0; i < 0; i++){
//need to implement finding items in OrderTicket and deleting them
    }

    //recalculate order
//this doesnt work until MenuItem is fixed
    //calculateTotals(ticket.calculateOrder());
//workaround - just pull the subtotal from the gui and subtract current item
    double price = item.toDouble();
    QString subtotal = ui->SubtotalText->text();
    subtotal.remove(0,1);
    double subt = subtotal.split(", ")[0].toDouble();
    calculateTotals(subt - price);
    delete rem;
}

void OrderForm::on_CashOutButton_clicked()
{
    //finalize the order and print recipts
}

void OrderForm::on_CancelOrderButton_clicked()
{
    //void out order, print a void recipt?
}

void OrderForm::on_SaveOrderButton_clicked()
{
    //set order to standby
}

void OrderForm::on_custSelectSpin_valueChanged(int custNum)
{
    double total;
    //this will recalculate the order totals
    if(custNum == 0){
        //calculate total of entire bill
        total = ticket.calculateOrder();
    }
    else{
        //calculate total for selected customer - 1
        total = ticket.calculateOrder(custNum-1);
        //if customer number does not exist, stop processing and return
        if (total == -1)
            return;
    }
}

/************************************************************************
 * setters and getters
 ************************************************************************/

int OrderForm::getTableNum(){
    QString numStr = ui->tableNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setTableNum(int table){
    QString tableStr = QString::number(table);
    ui->tableNumLabel->setText(tableStr);
    ui->tableNumLabel->hide();
}

int OrderForm::getEmpNum(){
    QString numStr = ui->empNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setEmpNum(int emp){
    QString empStr = QString::number(emp);
    ui->empNumLabel->setText(empStr);
    ui->empNumLabel->hide();
}

/************************************************************************
 * other private methods
 ************************************************************************/

void OrderForm::showOrderPage(){
    int tableNo = getTableNum();
//    printf("the int passed to showOrderPage is %i\n", tableNo);
    QString tabletext = " Table #";
    tabletext += QString::number(tableNo);
//    printf("the new text is %s\n", tabletext.toUtf8().constData());
    ui->TableText->setText(tabletext);
    QString title = "Order for" + tabletext;
    setWindowTitle(title);
    this->show();

}

void OrderForm::calculateTotals(double subtotal){
    double taxed = subtotal * TAX;
    double totaled = subtotal + taxed;
    QString temp = "$" + QString::number(subtotal,'f',2);
    ui->SubtotalText->setText(temp);
    temp = "$" + QString::number(taxed,'f',2);
    ui->TaxText->setText(temp);
    temp = "$" + QString::number(totaled,'f',2);
    ui->GrantTotalText->setText(temp);
}
