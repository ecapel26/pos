#include "OrderTicket.h"

/*  int order_id, tableNum;
    double order_Time_In;
    //this vector translates to customer<MenuItem<quantity>>>
    std::vector< std::vector< std::vector<MenuItem> > > order;
    Employee emp;*/
/* constructors */
OrderTicket::OrderTicket(){

}

OrderTicket::~OrderTicket(){
//        delete[] order;
}

/* mutators */
void OrderTicket::setOrderID(int idNum){
    order_id = idNum;
}

void OrderTicket::setEmp(Employee e){
    emp = e;
}

void OrderTicket::setTableNum(int num){
    tableNum = num;
}

//    void OrderTicket::setItemId(int); //should this be handled by the MenuItem
void OrderTicket::setQuantity(int cust, int itemID, int quant){

}

//    void OrderTicket::setItemDesc(std::string); //should this be handled by the MenuItem
void OrderTicket::setOrderTime(double time){
    order_Time_In = time;
}

/* accessors */
int OrderTicket::getOrderId(){
    return order_id;
}

int OrderTicket::getEmpNum(){
    return emp.getEmpNum();
}

Employee OrderTicket::getEmp(){
    return emp;
}

int OrderTicket::getTableNum(){
    return tableNum;
}

//    int OrderTicket::getItemId(int cust, int orderIndex);
//    int OrderTicket::getQuantity(int cust, int itemID);
//    std::string OrderTicket::getItemDesc(int cust, int orderIndex);
double OrderTicket::getOrderTime(){
    return order_Time_In;
}

void OrderTicket::addItemToOrder(int cust, MenuItem item){
    if(cust < order.size()){
        order[cust].push_back(item);
    }
}

void OrderTicket::removeItem(int cust, int itemId){
//remove the menu item from order[cust] vector
//need to search the id to find the item to remove
    for(std::vector<MenuItem>::iterator i = order[cust].begin(); i != order[cust].end(); i++){
        MenuItem item = *i;
        int id = item.getItemId();
        if(itemId == id){
//error: no matching function for call to 'std::vector<std::vector<MenuItem> >::erase(MenuItem&)'
            //order.erase(*i);
        }
    }
}

double OrderTicket::calculateOrder(){
    double total = 0.0;
    for(int unsigned cust = 0; cust < order.size(); cust++){
        for(int unsigned item = 0; item < order[cust].size(); item++){
            MenuItem temp = order[cust][item];
            total += temp.getItemPrice();
        }
    }
    return total;
}

double OrderTicket::calculateOrder(int cust){
    if(cust >= order.size())
        return -1;
    double total = 0.0;
    for(int unsigned item = 0; item < order[cust].size(); item++){
        MenuItem temp = order[cust][item];
        total += temp.getItemPrice();
    }

    return total;
}
